# Introduction

This repo serves as a demonstration of the inconsistent behaviour of Tensorflow 
on a Mac M1 when run in a CPU vs when run in a GPU.

My particular setup involves a MacBook Pro (13-inch, M1, 2020) computer running
macOS 12.0.1 and python 3.9 from miniforge.

**UPDATE** This problem no longer exists with tensorflow 2.8.0. The instructions
below will now produce correct inference results in metal.

[TOC]

# Environment setup

The following instructions have been adapted from Apple's original instructions
available [here](https://developer.apple.com/metal/tensorflow-plugin/).

* Make sure that Git & Git LFS are installed and configured in your system.
* Install [Miniforge](https://github.com/conda-forge/miniforge)
* Activate your base environment:
```bash
source [MINIFORGE_INSTALL_PATH]/bin/activate
```
* Create an environment and install the required dependencies:
```bash
conda create -n tf_test python=3.9
conda activate tf_test
conda install -c apple tensorflow-deps==2.8.0
pip install -r requirements.txt
```

# Running the code

Within the newly-created environment, you can run the code executing the following:

```bash
python main.py
```

In my setup, this produces the following output:

```
(tf_test) ➜  tensorflow-metal-cpu-vs-gpu-inference-incoherence git:(main) ✗ python main.py    
Tensorflow configuration:
	Version: 2.8.0
	Devices usable by Tensorflow:
		PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU')
		PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')
Metal device set to: Apple M1

systemMemory: 16.00 GB
maxCacheSize: 5.33 GB

2022-03-07 11:39:57.461373: I tensorflow/core/common_runtime/pluggable_device/pluggable_device_factory.cc:305] Could not identify NUMA node of platform GPU ID 0, defaulting to 0. Your kernel may not have been built with NUMA support.
2022-03-07 11:39:57.461648: I tensorflow/core/common_runtime/pluggable_device/pluggable_device_factory.cc:271] Created TensorFlow device (/job:localhost/replica:0/task:0/device:GPU:0 with 0 MB memory) -> physical PluggableDevice (device: 0, name: METAL, pci bus id: <undefined>)
2022-03-07 11:39:58.035299: W tensorflow/core/platform/profile_utils/cpu_utils.cc:128] Failed to get CPU frequency: 0 Hz
2022-03-07 11:39:58.125714: I tensorflow/core/grappler/optimizers/custom_graph_optimizer_registry.cc:113] Plugin optimizer for device_type GPU is enabled.
Model Evaluation on CPU
	Prediction: 4.890502452850342
2022-03-07 11:39:58.224371: I tensorflow/core/grappler/optimizers/custom_graph_optimizer_registry.cc:113] Plugin optimizer for device_type GPU is enabled.
Model Evaluation on GPU
	Prediction: 4.890533447265625
```

# Expected results

The expected result of the inference (and the one coherent with evaluating the model 
in Linux with & without CUDA) is `4.89`, with the Metal-based backend providing
coherent results.
