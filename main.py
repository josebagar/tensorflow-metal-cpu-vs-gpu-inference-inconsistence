#!/usr/bin/env python3

import pathlib
import numpy as np
import tensorflow as tf
from tensorflow import keras


def main(model_path, dataset_path):
    # Print some system info
    print('Tensorflow configuration:')
    print(f'\tVersion: {tf.__version__}')
    print('\tDevices usable by Tensorflow:')
    for device in tf.config.get_visible_devices():
        print(f'\t\t{device}')

    # Load the model & the input data
    model = keras.models.load_model(model_path)
    matrix_data = np.genfromtxt(dataset_path)
    matrix_data = matrix_data.reshape([1, matrix_data.shape[0], matrix_data.shape[1]])

    # Perform inference in CPU
    with tf.device('/CPU:0'):
        prediction = model.predict(matrix_data)[1]
        print('Model Evaluation on CPU')
        print(f'\tPrediction: {prediction[0, 0]}')

    # Perform inference in GPU
    with tf.device('/GPU:0'):
        prediction = model.predict(matrix_data)[1]
        print('Model Evaluation on GPU')
        print(f'\tPrediction: {prediction[0, 0]}')


if __name__ == "__main__":
    main('model/model.h5', 'dataset/01.csv')
